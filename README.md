# README

Estes documento README tem como objetivo fornecer as informações sobre como foi desenvolvido e como rodar o projeto Empresas.

### O que foi feito:

Seguindo as instruções do [README original](https://bitbucket.org/ioasys/empresas-backend/src/master/)
Foi desenvolvido uma API para listar empresas de acordo com filtros e endpoints.

* Seguem os endpoints disponíveis:
	* Listagem de Empresas: `/enterprises`
	* Detalhamento de Empresas: `/enterprises/{id}`
	* Filtro de Empresas por nome e tipo: `/enterprises?enterprise_types={type}&name={name}`
    
Para conseguir realizar as consultas, antes será necessário realizar login e, posteriormente, passar os custom headers de acordo com o informado no [README original](https://bitbucket.org/ioasys/empresas-backend/src/master/)

### Como rodar o projeto: 
 _OBS: Não foi configurado nenhum seed, então será necessário inserir os dados manualmente no DB._
 
 ***Antes de rodar, tenha certeza que você tem o Node Js instalado na sua maquina***

* Clone esse repositório e navegue até a pasta app
* Então rode  __yarn__ ou __npm i__
* Configure a conexão com o PostgreSQL:
    -  Você vai precisar criar o banco de dados manualmente, então conecte-se ao seu postgreSQL Server e rode o comando para criar o banco de dados.
   
```sql
          CREATE DATABASE "enterprise-db"; --ou qualquer outro nome que voce desejar
``` 
* 
    
    -  Abra o arquivo **dbConfig.ts** em `app/src/config/db/dbConfig.ts`
    -  Mude os seguintes valores na de acordo com sua connection string: 

```javascript
         
         //dbConfig.ts
          export const connectionConfig: DbConfig = {
            DATABASE_HOST: 'localhost',
            DATABASE_USER: 'postgres',
            DATABASE_PORT: 5432,
            DATABASE_PASSWORD: 'postgres',
            DATABASE_DB: 'enterprise-db', // o nome do banco que voce criou acima
          };
```
 


* Para adicionar os dados de exemplo, acesse a pasta _scripts_ no caminho `app/src/scripts`
* Depois **rode os scripts na ordem a seguir** :
    1. `user.sql` 
    2. `investor.sql`
    3. `enterpriseType.sql`
    4. `enterprise.sql`
        
* Finalmente você poderá rodar __yarn debug__ ou __npm run debug__ para rodar em modo debug com nodemon ou  __yarn start__ ou __npm start__ para iniciar a aplicação
 