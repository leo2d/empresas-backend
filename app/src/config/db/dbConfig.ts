export interface DbConfig {
  DATABASE_HOST: string;
  DATABASE_USER: string;
  DATABASE_PORT: number;
  DATABASE_PASSWORD: string;
  DATABASE_DB: string;
}

export const connectionConfig: DbConfig = {
  DATABASE_HOST: 'localhost',
  DATABASE_USER: 'postgres',
  DATABASE_PORT: 5432,
  DATABASE_PASSWORD: 'postgres',
  DATABASE_DB: 'enterprise-db',
};
