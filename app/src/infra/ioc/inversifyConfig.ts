import { Container, AsyncContainerModule } from 'inversify';
import { Repository } from 'typeorm';

import InjectTYPES from '../../constants/types/injectTypes';
import { connectionConfig } from '../../config/db/dbConfig';
import EnterpriseService from '../../domain/enterprise/services/enterpriseService';
import UserService from '../../domain/user/services/userService';
import Enterprise from '../../domain/enterprise/enterprise';
import User from '../../domain/user/user';
import Investor from '../../domain/investor/investor';
import { getDbConnection } from '../data/getConnection';
import { getEnterpriseRepository } from '../data/repositories/enterpriseRepositoy';
import { getInvestorRepository } from '../data/repositories/investorRepository';
import { getUserRepository } from '../data/repositories/userRepository';

const bindings = new AsyncContainerModule(async bind => {
  await getDbConnection(connectionConfig);

  bind<EnterpriseService>(InjectTYPES.services.EnterpriseService)
    .to(EnterpriseService)
    .inRequestScope();
  bind<UserService>(InjectTYPES.services.UserService)
    .to(UserService)
    .inRequestScope();

  bind<Repository<Enterprise>>(InjectTYPES.repositories.EnterpriseRepository)
    .toDynamicValue(() => {
      return getEnterpriseRepository();
    })
    .inRequestScope();
  bind<Repository<User>>(InjectTYPES.repositories.UserRepository)
    .toDynamicValue(() => {
      return getUserRepository();
    })
    .inRequestScope();
  bind<Repository<Investor>>(InjectTYPES.repositories.Investorpository)
    .toDynamicValue(() => {
      return getInvestorRepository();
    })
    .inRequestScope();
});

const setupContainer = async (): Promise<Container> => {
  const newContainer = new Container();

  await newContainer.loadAsync(bindings);

  return newContainer;
};

export default setupContainer;
