import { getConnection, Repository } from 'typeorm';
import Investor from '../../../domain/investor/investor';

export function getInvestorRepository(): Repository<Investor> {
  const conn = getConnection();
  const investorRepository = conn.getRepository(Investor);
  return investorRepository;
}
