import { getConnection, Repository } from 'typeorm';
import Enterprise from '../../../domain/enterprise/enterprise';

export function getEnterpriseRepository(): Repository<Enterprise> {
  const conn = getConnection();
  const personRepository = conn.getRepository(Enterprise);
  return personRepository;
}
