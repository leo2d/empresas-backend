import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import authConfig from '../../config/auth/authConfig';
import { getUserRepository } from '../../infra/data/repositories/userRepository';
import { stringIsValid } from '../../utils/stringUtils';

export default async function authMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<NextFunction> {
  try {
    const token = req.headers['access-token'] as string;
    const uid = req.headers['uid'] as string;
    const client = req.headers['client'] as string;
    if (
      !stringIsValid(token) ||
      !stringIsValid(client) ||
      !stringIsValid(uid)
    ) {
      res
        .status(401)
        .json({ errors: ['You need to sign in or sign up before continuing'] })
        .send();
      return;
    }

    const jwtPayload = jwt.verify(token, authConfig.scretKey) as any;
    res.locals.jwtPayload = jwtPayload;

    const { userId } = jwtPayload;
    const userRepository = getUserRepository();

    const user = await userRepository.findOne({ id: userId });

    let authSuccess = false;

    if (user) {
      if (user?.client === client && user.email === uid) {
        authSuccess = true;
      }
    }
    if (authSuccess) {
      res.set({
        'access-token': user.token,
        client: user.client,
        uid: user.email,
      });
      next();
    } else {
      res
        .status(401)
        .json({ errors: ['You need to sign in or sign up before continuing'] })
        .send();
      return;
    }
  } catch (error) {
    res
      .status(500)
      .json(error)
      .send();
    return;
  }
}
