import {
  controller,
  httpPost,
  response,
  request,
  requestBody,
  interfaces,
} from 'inversify-express-utils';
import { inject } from 'inversify';
import { Request, Response, NextFunction } from 'express';
import SigInResponse from '../responses/user/sigInResponse';
import SigInErrorResponse from '../responses/user/sigInErrorResponse';
import { mapBodyToLoginDTO, mapToInvestorDTO } from '../mappers/userMappings';
import { stringIsValid } from '../../utils/stringUtils';
import InjectTYPES from '../../constants/types/injectTypes';
import UserService from '../../domain/user/services/userService';

@controller('/users')
export default class UserController implements interfaces.Controller {
  private userService: UserService;

  constructor(
    @inject(InjectTYPES.services.UserService)
    userService: UserService
  ) {
    this.userService = userService;
  }

  @httpPost('/auth/sign_in')
  async signIn(
    @request() req: Request,
    @requestBody() body: any,
    @response() res: Response
  ): Promise<void> {
    try {
      const clientInfo = req.headers['user-agent'];
      const loginDTO = mapBodyToLoginDTO(body);

      if (!stringIsValid(body?.email) || !stringIsValid(body?.password)) {
        this.returnInvalidCredentials(res);
      } else {
        const investorUser = await this.userService.sigIn(loginDTO, clientInfo);

        if (investorUser) {
          const { user } = investorUser;

          res.set({
            'access-token': user.token,
            client: user.client,
            uid: user.email,
          });

          const dto = mapToInvestorDTO(investorUser);

          res.status(200).json(new SigInResponse(dto, null, true));
        } else {
          this.returnInvalidCredentials(res);
        }
      }
    } catch (error) {
      res.status(500).json(error.message);
    }
  }

  private returnInvalidCredentials(res: Response): void {
    res
      .status(401)
      .json(
        new SigInErrorResponse(false, [
          'Invalid login credentials. Please try again.',
        ])
      );
  }
}
