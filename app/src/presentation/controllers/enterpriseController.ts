import { Response } from 'express';
import {
  interfaces,
  controller,
  httpGet,
  response,
  requestParam,
  queryParam,
} from 'inversify-express-utils';
import { inject } from 'inversify';

import InjectTYPES from '../../constants/types/injectTypes';
import EnterpriseService from '../../domain/enterprise/services/enterpriseService';
import FilterDTO from '../../domain/shared/dots/filterDTO';
import EnterpriseListResponse from '../responses/enterprise/enterpriseListResponse';
import {
  mapTolistItemDTO,
  mapToDetailsDTO,
} from '../mappers/enterpriseMappings';
import EnterPriseDetailsResponse from '../responses/enterprise/enterpriseDetailsResponse';
import ErrorResponse from '../responses/errorResponse';
import authMiddleware from '../middlewares/authMiddleware';

@controller('/enterprises')
export default class PersonController implements interfaces.Controller {
  public enterpriseService: EnterpriseService;

  constructor(
    @inject(InjectTYPES.services.EnterpriseService)
    enterpriseService: EnterpriseService
  ) {
    this.enterpriseService = enterpriseService;
  }

  @httpGet('/:id', authMiddleware)
  async getbyId(
    @response() res: Response,
    @requestParam() id: number
  ): Promise<void> {
    try {
      const result = await this.enterpriseService.getById(id);
      if (result) {
        const dto = mapToDetailsDTO(result);
        res.status(200).json(new EnterPriseDetailsResponse(dto, true));
      } else {
        res.status(404).json(new ErrorResponse(404, 'Not Found'));
      }
    } catch (error) {
      res.status(500).json(error.message);
    }
  }

  @httpGet('/', authMiddleware)
  async getbyFilter(
    @response() res: Response,
    @queryParam() filter: FilterDTO
  ): Promise<void> {
    try {
      const results = await this.enterpriseService.getByFilter(filter);

      const dtos = results?.map(item => mapTolistItemDTO(item));

      res.status(200).json(new EnterpriseListResponse(dtos));
    } catch (error) {
      res.status(500).json(error.message);
    }
  }
}
