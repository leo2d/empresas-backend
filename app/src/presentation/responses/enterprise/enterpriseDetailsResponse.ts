import EnterpriseDetailsDTO from '../../../domain/enterprise/dtos/enterpriseDetailsDTO';

export default class EnterPriseDetailsResponse {
  enterprise: EnterpriseDetailsDTO;
  success: boolean;

  constructor(enterprise: EnterpriseDetailsDTO, success: boolean) {
    this.enterprise = enterprise;
    this.success = success;
  }
}
