import EnterpriseDTO from '../../../domain/enterprise/dtos/enterpriseDTO';

export default class EnterpriseListResponse {
  enterprises: EnterpriseDTO[];

  constructor(enterprises: EnterpriseDTO[]) {
    this.enterprises = enterprises;
  }
}
