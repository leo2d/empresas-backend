import InvestorDTO from '../../../domain/investor/dtos/investorDTO';

export default class SigInResponse {
  investor: InvestorDTO;
  enterprise: any;
  success: boolean;

  constructor(investor: InvestorDTO, enterprise: any, success: boolean) {
    this.investor = investor;
    this.enterprise = enterprise;
    this.success = success;
  }
}
