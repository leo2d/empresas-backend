/* eslint-disable @typescript-eslint/camelcase */
import Investor from '../../domain/investor/investor';
import InvestorDTO from '../../domain/investor/dtos/investorDTO';
import LoginDTO from '../../domain/user/dtos/loginDTO';

export const mapToInvestorDTO = (investor: Investor): InvestorDTO => {
  const investorPortifolio = investor.getPortifolio();
  const investorDTO: InvestorDTO = {
    id: investor.id,
    investor_name: investor.investorName,
    email: investor.user.email,
    city: investor.city,
    country: investor.country,
    balance: investor.balance,
    photo: investor.photo,
    portfolio: {
      enterprises: investorPortifolio.enterprises,
      enterprises_number: investorPortifolio.enterprisesNumber,
    },
    portfolio_value: investor.portfolioValue,
    first_access: investor.user.firstAccess,
    super_angel: investor.superAngel,
  };
  return investorDTO;
};

export const mapBodyToLoginDTO = (body: any): LoginDTO => {
  return {
    email: body.email,
    password: body.password,
  };
};
