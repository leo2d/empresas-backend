/* eslint-disable @typescript-eslint/camelcase */
import Enterprise from '../../domain/enterprise/enterprise';
import EnterpriseDTO from '../../domain/enterprise/dtos/enterpriseDTO';
import EnterpriseDetailsDTO from '../../domain/enterprise/dtos/enterpriseDetailsDTO';
import EnterpriseTypeDTO from '../../domain/enterprise/dtos/enterpriseTypeDTO';
import EnterpriseType from '../../domain/enterprise/entities/enterpriseType';

const mapEnterPriseTypeToDTO = (type: EnterpriseType): EnterpriseTypeDTO => {
  return {
    id: type.id,
    enterprise_type_name: type.enterpriseTypeName,
  };
};

export const mapTolistItemDTO = (enterprise: Enterprise): EnterpriseDTO => {
  const listItem: EnterpriseDTO = {
    id: enterprise.id,
    email_enterprise: enterprise.emailEnterprise,
    facebook: enterprise.facebook,
    twitter: enterprise.twitter,
    linkedin: enterprise.linkedin,
    phone: enterprise.phone,
    own_enterprise: enterprise.ownEnterprise,
    enterprise_name: enterprise.enterpriseName,
    photo: enterprise.photo,
    description: enterprise.description,
    city: enterprise.city,
    country: enterprise.country,
    value: enterprise.value,
    share_price: enterprise.sharePrice,
    enterprise_type: mapEnterPriseTypeToDTO(enterprise.enterpriseType),
  };

  return listItem;
};

export const mapToDetailsDTO = (
  enterprise: Enterprise
): EnterpriseDetailsDTO => {
  const detail: EnterpriseDetailsDTO = {
    id: enterprise.id,
    enterprise_name: enterprise.enterpriseName,
    description: enterprise.description,
    email_enterprise: enterprise.emailEnterprise,
    facebook: enterprise.facebook,
    twitter: enterprise.twitter,
    linkedin: enterprise.linkedin,
    phone: enterprise.phone,
    own_enterprise: enterprise.ownEnterprise,
    photo: enterprise.photo,
    value: enterprise.value,
    shares: enterprise.shares,
    share_price: enterprise.sharePrice,
    own_shares: enterprise.ownShares,
    city: enterprise.city,
    country: enterprise.country,
    enterprise_type: mapEnterPriseTypeToDTO(enterprise.enterpriseType),
  };

  return detail;
};
