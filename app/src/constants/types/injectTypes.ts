const InjectTYPES = {
  services: {
    EnterpriseService: Symbol('EnterpriseService'),
    UserService: Symbol('UserService'),
    InvestorService: Symbol('InvestorService'),
  },
  repositories: {
    EnterpriseRepository: Symbol('EnterpriseRepository'),
    UserRepository: Symbol('Userepository'),
    Investorpository: Symbol('Investorpository'),
  },
};

export default InjectTYPES;
