import { Repository, FindConditions, Like } from 'typeorm';
import { inject, injectable } from 'inversify';
import InjectTYPES from '../../../constants/types/injectTypes';
import Enterprise from '../enterprise';
import FilterDTO from '../../shared/dots/filterDTO';
import { trimAndLower } from '../../../utils/stringUtils';
import { ILike } from '../../shared/findOperatorWithExtras';

@injectable()
export default class EnterpriseService {
  private enterpriseRepository: Repository<Enterprise>;

  private enterpriseTypeRelation = 'enterpriseType';

  constructor(
    @inject(InjectTYPES.repositories.EnterpriseRepository)
    enterpriseRepository: Repository<Enterprise>
  ) {
    this.enterpriseRepository = enterpriseRepository;
  }

  private makeEnterPriseConditions(
    filter: FilterDTO
  ): FindConditions<Enterprise> {
    let conditions: FindConditions<Enterprise>;

    if (filter.name) {
      conditions = {
        ...conditions,
        enterpriseName: ILike(`%${trimAndLower(filter.name)}%`),
      };
    }
    if (filter.enterprise_types && filter.enterprise_types > 0) {
      conditions = {
        ...conditions,
        enterpriseType: { id: filter.enterprise_types },
      };
    }

    return conditions;
  }

  async getByFilter(filter: FilterDTO): Promise<Enterprise[]> {
    const where = this.makeEnterPriseConditions(filter);

    const results = await this.enterpriseRepository.find({
      relations: [this.enterpriseTypeRelation],
      where,
    });

    return results;
  }

  async getById(enterpriseId: number): Promise<Enterprise> {
    const enterprise = await this.enterpriseRepository.findOne(enterpriseId, {
      relations: [this.enterpriseTypeRelation],
    });

    return enterprise;
  }

  async get(): Promise<Enterprise[]> {
    const results = await this.enterpriseRepository.find({
      relations: [this.enterpriseTypeRelation],
    });

    return results;
  }
}
