import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';
import BaseEntity from '../shared/entities/baseEntity';
import EnterpriseType from './entities/enterpriseType';

@Entity('enterprise')
export default class Enterprise extends BaseEntity {
  @Column({ name: 'email_enterprise', type: 'varchar', nullable: true })
  emailEnterprise?: string;

  @Column({ type: 'varchar', nullable: true })
  facebook?: string;

  @Column({ type: 'varchar', nullable: true })
  twitter?: string;

  @Column({ type: 'varchar', nullable: true })
  linkedin?: string;

  @Column({ type: 'varchar', nullable: true })
  phone?: string;

  @Column({ type: 'varchar', nullable: true })
  photo?: string;

  @Column({
    name: 'own_enterprise',
    type: 'boolean',
    nullable: false,
    default: false,
  })
  ownEnterprise: boolean;

  @Column({ name: 'enterprise_name', type: 'varchar', nullable: false })
  enterpriseName: string;

  @Column({ type: 'varchar', nullable: false })
  description: string;

  @Column({ type: 'varchar', nullable: false })
  city: string;

  @Column({ type: 'varchar', nullable: false })
  country: string;

  @Column({ type: 'integer', nullable: false })
  value: number;

  @Column({ type: 'integer', nullable: false })
  shares: number;

  @Column({ name: 'own_shares', type: 'integer', nullable: false })
  ownShares: number;

  @Column({ name: 'share_price', type: 'decimal', nullable: false })
  sharePrice: number;

  @Column({ name: 'enterprise_type_id', type: 'integer', nullable: false })
  enterpriseTypeId: number;

  @ManyToOne(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    type => EnterpriseType,
    enterprisetype => enterprisetype.enterprises,
    { nullable: false }
  )
  @JoinColumn({ name: 'enterprise_type_id', referencedColumnName: 'id' })
  enterpriseType: EnterpriseType;
}
