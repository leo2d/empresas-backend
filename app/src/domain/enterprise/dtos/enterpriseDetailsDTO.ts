import EnterpriseDTO from './enterpriseDTO';

export default interface EnterpriseDetailsDTO extends EnterpriseDTO {
  own_shares: number;
  shares: number;
}
