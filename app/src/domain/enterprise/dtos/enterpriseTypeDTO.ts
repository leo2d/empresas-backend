export default interface EnterpriseTypeDTO {
  id: number;
  enterprise_type_name: string;
}
