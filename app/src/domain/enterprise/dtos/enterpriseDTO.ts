import EnterpriseTypeDTO from './enterpriseTypeDTO';

export default interface EnterpriseDTO {
  id: number;
  email_enterprise?: string;
  facebook?: string;
  twitter?: string;
  linkedin?: string;
  phone?: string;
  photo?: string;
  own_enterprise: boolean;
  enterprise_name: string;
  description: string;
  city: string;
  country: string;
  value: number;
  share_price: number;
  enterprise_type: EnterpriseTypeDTO;
}
