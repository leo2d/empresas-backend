import { Column, Entity, OneToMany } from 'typeorm';
import BaseEntity from '../../shared/entities/baseEntity';
import Enterprise from '../enterprise';

@Entity()
export default class EnterpriseType extends BaseEntity {
  @Column({ name: 'enterprise_type_name', type: 'varchar', nullable: false })
  enterpriseTypeName: string;

  @OneToMany(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    type => Enterprise,
    ent => ent.enterpriseType
  )
  enterprises: Enterprise[];
}
