export default interface FilterDTO {
  enterprise_types?: number;
  name?: string;
}
