import { Column, Entity } from 'typeorm';
import { sign } from 'jsonwebtoken';
import * as crypto from 'crypto';
import BaseEntity from '../shared/entities/baseEntity';
import { compare } from 'bcryptjs';

@Entity()
export default class User extends BaseEntity {
  @Column({ type: 'varchar', nullable: false })
  email: string;
  @Column({ type: 'varchar', nullable: false })
  password: string;
  @Column({ type: 'varchar', nullable: true })
  token: string;
  @Column({ type: 'varchar', nullable: true })
  client: string;
  @Column({
    name: 'first_access',
    type: 'boolean',
    nullable: true,
  })
  firstAccess?: boolean;

  setFirstAccess(): void {
    if (typeof this.firstAccess === 'undefined' || this.firstAccess === null)
      this.firstAccess = true;
    else this.firstAccess = false;
  }

  sigIn(key: string, expirationTime: string, clientInfo: string): void {
    const token = sign({ userId: this.id }, key, { expiresIn: expirationTime });
    this.token = token;

    const clientHash = crypto
      .createHash('sha256')
      .update(`${clientInfo}-${new Date()}`)
      .digest('base64');

    this.client = clientHash;
  }

  async checkPassword(inputPassWord: string): Promise<boolean> {
    const isPasswordMatch = await compare(inputPassWord, this.password);
    return isPasswordMatch;
  }
}
