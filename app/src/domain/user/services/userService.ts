import { injectable, inject } from 'inversify';
import { Repository } from 'typeorm';
import User from '../user';
import InjectTYPES from '../../../constants/types/injectTypes';
import Investor from '../../investor/investor';
import LoginDTO from '../dtos/loginDTO';
import { ILike } from '../../shared/findOperatorWithExtras';
import { trimAndLower } from '../../../utils/stringUtils';
import authConfig from '../../../config/auth/authConfig';

@injectable()
export default class UserService {
  private investorRepository: Repository<Investor>;
  private userRepository: Repository<User>;

  constructor(
    @inject(InjectTYPES.repositories.Investorpository)
    investorRepository: Repository<Investor>,
    @inject(InjectTYPES.repositories.UserRepository)
    userRepository: Repository<User>
  ) {
    this.userRepository = userRepository;
    this.investorRepository = investorRepository;
  }

  async sigIn(loginDTO: LoginDTO, clientInfo: string): Promise<Investor> {
    const user = await this.userRepository.findOne({
      where: {
        email: ILike(`%${trimAndLower(loginDTO.email)}%`),
      },
    });

    if (!user) return null;

    const isPasswordMatch = await user.checkPassword(loginDTO.password);
    if (!isPasswordMatch) return null;

    user.setFirstAccess();
    user.sigIn(authConfig.scretKey, authConfig.expiresIn, clientInfo);

    await this.userRepository.update({ id: user.id }, user);

    const investor = await this.getInvestorByUserId(user.id);
    investor.setPortifolioValue();

    return investor;
  }

  async getInvestorByUserId(userId: number): Promise<Investor> {
    return await this.investorRepository.findOne({
      relations: ['user', 'enterprises'],
      where: { userId },
    });
  }
}
