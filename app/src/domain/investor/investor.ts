import {
  Column,
  Entity,
  ManyToOne,
  ManyToMany,
  JoinTable,
  JoinColumn,
  OneToOne,
} from 'typeorm';

import BaseEntity from '../shared/entities/baseEntity';
import Portfolio from './entities/portifolio';
import Enterprise from '../enterprise/enterprise';
import User from '../user/user';

@Entity()
export default class Investor extends BaseEntity {
  @Column({ name: 'investor_name', type: 'varchar', nullable: false })
  investorName: string;

  @Column({ type: 'varchar', nullable: false })
  city: string;

  @Column({ type: 'varchar', nullable: false })
  country: string;

  @Column({ type: 'varchar', nullable: true })
  photo: string;

  @Column({ type: 'decimal', nullable: false })
  balance: number;

  // @Column({ name: 'portfolio_value', type: 'decimal', nullable: false })
  portfolioValue: number;

  @Column({
    name: 'super_angel',
    type: 'boolean',
    nullable: false,
    default: false,
  })
  superAngel: boolean;

  @Column({ name: 'user_id', type: 'integer', nullable: false })
  userId: number;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @OneToOne(type => User)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: User;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  @ManyToMany(type => Enterprise)
  @JoinTable()
  enterprises: Enterprise[];

  private portfolio: Portfolio;

  getPortifolio(): Portfolio {
    if (!this.portfolio) {
      this.portfolio = new Portfolio(this.enterprises);
    }
    return this.portfolio;
  }

  setPortifolioValue(): void {
    if (this.enterprises && this.enterprises.length) {
      this.portfolioValue =
        this.balance +
        this.enterprises.reduce((acc, curr) => curr.sharePrice + acc, 0);
    } else {
      this.portfolioValue = this.balance;
    }
  }
}
