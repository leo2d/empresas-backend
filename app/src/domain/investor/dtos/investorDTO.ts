export default interface InvestorDTO {
  id: number;
  investor_name: string;
  email: string;
  city: string;
  country: string;
  balance: number;
  photo: string;
  portfolio: {
    enterprises_number: number;
    enterprises: any[];
  };
  portfolio_value: number;
  first_access: boolean;
  super_angel: boolean;
}
