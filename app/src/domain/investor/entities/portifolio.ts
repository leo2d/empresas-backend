import Enterprise from '../../enterprise/enterprise';

export default class Portfolio {
  constructor(investments: Enterprise[]) {
    this.enterprises = investments || [];
    this.enterprisesNumber = this.enterprises?.length || 0;
  }
  enterprisesNumber: number;
  enterprises: Enterprise[];
}
